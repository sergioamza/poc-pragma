console.log('Loading function');

const doc = require('dynamodb-doc');

const dynamo = new doc.DynamoDB();

exports.handler = (event, context, callback) => {
    //console.log('Received event:', JSON.stringify(event, null, 2));

    const done = (err, res) => callback(null, {
        statusCode: err ? '400' : '200',
        body: err ? err.message : res,
        headers: {
            'Content-Type': 'application/json',
        },
    });
    switch (event.httpMethod) {
        case 'DELETE':
            dynamo.deleteItem(JSON.parse(JSON.stringify(event.body)), done);
            break;
        case 'GET':
            console.log(JSON.stringify(event.body));
            if(event.queryStringParameters.hasOwnProperty('TableName')) {
                dynamo.scan({ TableName: event.queryStringParameters.TableName }, done);
            }
            else {
                dynamo.query(JSON.parse(JSON.stringify(event.body)), done);
            }
            break;
        case 'POST':
            dynamo.putItem(JSON.parse(JSON.stringify(event.body)), done);
            break;
        case 'PUT':
            event.body.UpdateExpression="SET ";
            event.body.ExpressionAttributeValues={};
            for (const prop in event.body.Item) {
                //console.log(`${prop} = ${event.body.Item[prop]}`);
                event.body.UpdateExpression=event.body.UpdateExpression + ` ${prop}=:${prop},`
                event.body.ExpressionAttributeValues[`:${prop}`]=`${event.body.Item[prop]}`;
            }
            event.body.UpdateExpression = event.body.UpdateExpression.substring(0,event.body.UpdateExpression.length-1)
            delete event.body.Item;
            //console.log(JSON.stringify(event.body));
            dynamo.updateItem(JSON.parse(JSON.stringify(event.body)), done);
            break;
        default:
            done(new Error(`Método no soportado en cliente "${event.httpMethod}"`));
    }
};



