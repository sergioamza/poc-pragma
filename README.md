# PoC Pragma

Con el propósito de poner en práctica y validar los conceptos adquiridos de la capacitación sobre la plataforma en la nube de Amazon Web Services (AWS).

Se propondrá un problema a manera de prueba de concepto (PoC), para construir API’s Restful que expongan como mínimo las operaciones CRUD de una tabla de base de datos llamada CLIENTE.

 

Los datos de un cliente son los siguientes:

- Nombres
- Apellidos
- Tipo y número de identificación
- Edad
- Ciudad de nacimiento


Normalmente las consultas se hacen mediante el tipo y número de identificación, pero en algunos casos es necesario consultar los clientes que sean mayor o igual a determinada edad. Tener presente esta información para el diseño de la tabla donde se va a almacenar la información de los clientes de forma tal que le permitan al servicio ser lo más rápido posible.

 

Cada operación debe ser una lambda independiente desarrollada usando el lenguaje de programación de su preferencia. Todos los servicios deben permitir Cross Domain o el consumo de recursos desde un dominio diferente para que la aplicación web ya existente los pueda consumir.

 

La arquitectura propuesta para esta PoC es la siguiente:

![Propuesta](ArquitecturaPropuesta.jpg "Arquitectura Propuesta")

Finalmente por favor enviar vía correo una colección de servicios Restful que expongan el CRUD para poder probarlos. Además incluir el código fuente de su solución en un archivo <nombre_completo>.zip ó un enlace a algún repositorio.


